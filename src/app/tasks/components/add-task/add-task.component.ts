import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { Task } from 'src/app/tasks/models/task.model';
import { TaskService } from 'src/app/tasks/services/task.service';
import { MessageDialogService } from 'src/app/shared/modules/message-dialog/services/message-dialog.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss'],
})
export class AddTaskComponent implements OnInit {
  @Input() openedFromModal: boolean = false;
  @Output() cancelUpdate: EventEmitter<boolean> = new EventEmitter();
  @Output() addComplete: EventEmitter<void> = new EventEmitter();

  taskForm!: FormGroup;

  isTaskListNotEmpty$!: Observable<boolean>;

  constructor(
    private fb: FormBuilder,
    private taskService: TaskService,
    private messageDialogService: MessageDialogService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    this.isTaskListNotEmpty$ = this.taskService.isTaskListNotEmptySelector();
  }

  initializeForm() {
    this.taskForm = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.taskForm.valid) {
      const newTask: Task = {
        id: Math.floor(Math.random() * 10000),
        title: this.taskForm.get('title')!.value,
        description: this.taskForm.get('title')!.value,
        completed: false,
      };
      this.addTask(newTask);
    }
  }

  addTask(newTask: Task) {
    this.taskService.saveTask(newTask).subscribe({
      next: (message) => {
        this.taskForm.reset();
        if (this.openedFromModal) {
          this.addComplete.emit();
        }
        this.messageDialogService.openDialog('success');
      },
      error: (error) => {
        this.messageDialogService.openDialog('error');
      },
    });
  }

  cancelEdit() {
    this.cancelUpdate.emit();
  }
}
