import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

import { Task } from 'src/app/tasks/models/task.model';
import { TaskService } from 'src/app/tasks/services/task.service';
import { TaskDialogComponent } from 'src/app/tasks/components/task-dialog/task-dialog.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit {
  tasks$!: Observable<Task[]>;

  @ViewChild(TaskDialogComponent) dialog!: TaskDialogComponent;

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.tasks$ = this.taskService.getTasks();
  }

  openAddDialog() {
    if (this.dialog) {
      this.dialog.openDialog('add');
    }
  }
}
