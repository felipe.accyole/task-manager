import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Task } from 'src/app/tasks/models/task.model';
import { TaskService } from 'src/app/tasks/services/task.service';
import { TaskDialogComponent } from 'src/app/tasks/components/task-dialog/task-dialog.component';
import { MessageDialogService } from 'src/app/shared/modules/message-dialog/services/message-dialog.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskItemComponent implements OnInit {
  @Input() task!: Task;

  @ViewChild(TaskDialogComponent) dialog!: TaskDialogComponent;

  checkForm!: FormGroup;

  constructor(
    private taskService: TaskService,
    private fb: FormBuilder,
    private messageDialogService: MessageDialogService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.checkForm = this.fb.group({
      completed: [this.task.completed],
    });
  }

  deleteTask(taskId: number) {
    this.taskService.deleteTask(taskId).subscribe({
      next: (message) => {
        this.messageDialogService.openDialog('success');
      },
      error: (error) => {
        this.messageDialogService.openDialog('error');
      },
    });
  }

  onSubmit() {
    if (this.checkForm.valid) {
      const updatedtask: Task = {
        id: this.task.id,
        title: this.task.title,
        description: this.task.description,
        completed: this.checkForm.get('completed')!.value,
      };
      this.updateForm(this.task.id, updatedtask);
    }
  }

  updateForm(taskId: number, changes: Task) {
    this.taskService.updateTasks(taskId, changes);
  }

  openUpdateDialog() {
    if (this.dialog) {
      this.dialog.openDialog('edit');
    }
  }
}
