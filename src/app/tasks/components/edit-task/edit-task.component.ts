import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Task } from 'src/app/tasks/models/task.model';
import { TaskService } from 'src/app/tasks/services/task.service';
import { MessageDialogService } from 'src/app/shared/modules/message-dialog/services/message-dialog.service';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss'],
})
export class EditTaskComponent implements OnInit {
  @Input() task!: Task;
  @Output() cancelUpdate: EventEmitter<boolean> = new EventEmitter();

  editTaskForm!: FormGroup;

  constructor(
    private taskService: TaskService,
    private fb: FormBuilder,
    private messageDialogService: MessageDialogService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.editTaskForm = this.fb.group({
      title: [this.task.title, [Validators.required]],
      description: [this.task.description, [Validators.required]],
      completed: [this.task.completed, [Validators.required]],
    });
  }

  onSubmit() {
    if (this.editTaskForm.valid) {
      this.updateTask(this.task.id, this.editTaskForm.value);
    }
  }

  updateTask(taskId: number, changes: Task) {
    this.taskService.updateTasks(taskId, changes).subscribe({
      next: (message) => {
        this.messageDialogService.openDialog('success');
      },
      error: (error) => {
        this.messageDialogService.openDialog('error');
      },
    });
  }

  cancelEdit() {
    this.cancelUpdate.emit();
  }
}
