import { Component, ElementRef, Input, ViewChild } from '@angular/core';

import { Task } from 'src/app/tasks/models/task.model';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.scss'],
})
export class TaskDialogComponent {
  @Input() task!: Task;
  @ViewChild('dialog') dialog!: ElementRef<HTMLDialogElement>;

  dialogMode!: 'edit' | 'add';

  openDialog(mode: 'edit' | 'add') {
    this.dialogMode = mode;
    if (this.dialog.nativeElement) {
      this.dialog.nativeElement.showModal();
    }
  }

  closeDialog() {
    if (this.dialog && this.dialog.nativeElement) {
      this.dialog.nativeElement.close();
    }
  }
}
