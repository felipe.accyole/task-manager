import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskItemComponent } from './components/task-item/task.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { EditTaskComponent } from './components/edit-task/edit-task.component';
import { TaskDialogComponent } from './components/task-dialog/task-dialog.component';
import { MessageDialogModule } from 'src/app/shared/modules/message-dialog/message-dialog.module';

@NgModule({
  declarations: [
    TaskListComponent,
    TaskItemComponent,
    AddTaskComponent,
    EditTaskComponent,
    TaskDialogComponent,
  ],
  imports: [CommonModule, ReactiveFormsModule, MessageDialogModule],
  exports: [TaskListComponent, AddTaskComponent],
})
export class TasksModule {}
