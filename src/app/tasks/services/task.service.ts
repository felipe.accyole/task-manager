import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, map, of } from 'rxjs';

import { Task } from 'src/app/tasks/models/task.model';
import { MOCK_TASK_LIST } from 'src/app/shared/mock/task-list.mock';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  tasks: Task[] = MOCK_TASK_LIST;
  tasksSubject = new BehaviorSubject<Task[]>(this.tasks);
  tasksState: Observable<Task[]> = this.tasksSubject.asObservable();

  constructor() {}

  getTasks(): Observable<Task[]> {
    return this.tasksState;
  }

  saveTask(newTask: Task): Observable<any> {
    const tasks = this.tasksSubject.getValue();

    tasks.unshift(newTask);
    this.tasksSubject.next(tasks);

    return of('Task Saved').pipe(
      catchError((error) => {
        return of(error);
      })
    );
  }

  updateTasks(taskId: number, changes: Task): Observable<any> {
    const tasks = this.tasksSubject.getValue();
    const taskIndex = tasks.findIndex((task) => task.id == taskId);

    const newTasks = tasks.slice(0);

    newTasks[taskIndex] = {
      ...tasks[taskIndex],
      ...changes,
    };
    this.tasksSubject.next(newTasks);

    return of('Task Updated').pipe(
      catchError((error) => {
        return of(error);
      })
    );
  }

  deleteTask(taskId: number): Observable<any> {
    const tasks = this.tasksSubject.getValue();
    const taskIndex = tasks.findIndex((task) => task.id == taskId);

    tasks.splice(taskIndex, 1);
    this.tasksSubject.next(tasks);

    return of('Task deleted').pipe(
      catchError((error) => {
        return of(error);
      })
    );
  }

  isTaskListEmptySelector(): Observable<boolean> {
    const isEmpty: Observable<boolean> = this.tasksState.pipe(
      map((tasks) => tasks.length === 0)
    );

    return isEmpty;
  }

  isTaskListNotEmptySelector(): Observable<boolean> {
    const isNotEmpty: Observable<boolean> = this.tasksState.pipe(
      map((tasks) => tasks.length > 0)
    );

    return isNotEmpty;
  }
}
