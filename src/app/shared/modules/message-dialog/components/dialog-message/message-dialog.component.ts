import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { MessageDialogService } from 'src/app/shared/modules/message-dialog/services/message-dialog.service';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss'],
})
export class MessageDialogComponent implements OnInit {
  @ViewChild('dialogMessage') dialog!: ElementRef<HTMLDialogElement>;
  dialogType!: 'success' | 'error' | undefined;

  constructor(private messageDialogService: MessageDialogService) {}

  ngOnInit(): void {
    this.messageDialogService.dialogState$.subscribe((type) => {
      this.openDialog(type);
    });
  }

  openDialog(type: 'success' | 'error') {
    this.dialogType = type;
    if (this.dialog.nativeElement) {
      this.dialog.nativeElement.showModal();

      setTimeout(() => {
        this.closeDialog();
      }, 2000);
    }
  }

  closeDialog() {
    this.dialogType = undefined;

    if (this.dialog.nativeElement) {
      setTimeout(() => {
        this.dialog.nativeElement.close();
      }, 300);
    }
  }
}
