import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MessageDialogService {
  private dialogSubject = new Subject<'success' | 'error'>();

  dialogState$ = this.dialogSubject.asObservable();

  openDialog(mode: 'success' | 'error') {
    this.dialogSubject.next(mode);
  }
}
