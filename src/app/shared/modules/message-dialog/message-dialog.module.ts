import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageDialogComponent } from './components/dialog-message/message-dialog.component';
import { SuccessMessageComponent } from './components/success-message/success-message.component';
import { ErrorMessageComponent } from './components/error-message/error-message.component';

@NgModule({
  declarations: [
    MessageDialogComponent,
    SuccessMessageComponent,
    ErrorMessageComponent,
  ],
  imports: [CommonModule],
  exports: [MessageDialogComponent],
})
export class MessageDialogModule {}
