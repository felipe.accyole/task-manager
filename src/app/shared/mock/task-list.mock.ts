import { Task } from 'src/app/tasks/models/task.model';

export const MOCK_TASK_LIST: Task[] = [
  {
    id: 1,
    title: 'Complete o projeto em TypeScript',
    completed: false,
    description:
      'Termine de implementar todos os recursos necessários e certifique-se de que o projeto passa em todos os casos de teste.',
  },
  {
    id: 2,
    title: 'Leia as últimas atualizações da documentação',
    completed: true,
    description:
      'Leia as mudanças mais recentes na documentação para se manter atualizado com as últimas diretrizes e melhores práticas.',
  },
  {
    id: 3,
    title: 'Escreva testes unitários para o novo recurso',
    completed: false,
    description:
      'Desenvolva testes unitários abrangentes para cobrir o novo recurso, garantindo que todos os casos extremos sejam testados.',
  },
  {
    id: 4,
    title: 'Refatore a base de código antiga',
    completed: true,
    description:
      'Melhore a qualidade do código da base de código legada aplicando padrões de design modernos e melhores práticas.',
  },
  {
    id: 5,
    title: 'Organize os arquivos do projeto',
    completed: false,
    description:
      'Classifique e estruture os arquivos do projeto em diretórios apropriados para melhor manutenção e legibilidade.',
  },
];
