import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { TaskService } from 'src/app/tasks/services/task.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isTaskListEmpty$!: Observable<boolean>;

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.isTaskListEmpty$ = this.taskService.isTaskListEmptySelector();
  }
}
