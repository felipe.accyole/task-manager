import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './components/home/home.component';
import { TasksModule } from 'src/app/tasks/tasks.module';
import { NavBarModule } from 'src/app/shared/modules/nav-bar/nav-bar.module';
import { MessageDialogModule } from 'src/app/shared/modules/message-dialog/message-dialog.module';
@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, TasksModule, NavBarModule, MessageDialogModule],
})
export class HomeModule {}
