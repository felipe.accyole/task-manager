## Funcionalidades

- **Adicionar Tarefas:** Os usuários podem adicionar novas tarefas preenchendo um formulário com título e descrição.
- **Listar Tarefas:** As tarefas são listadas em uma interface amigável, com indicação de quais estão completas ou não.
- **Atualizar Tarefas:** Os usuários podem atualizar o status e as informações de uma tarefa existente.
- **Excluir Tarefas:** As tarefas podem ser removidas da lista.
- **Mensagens de Sucesso e Erro:** Mensagens de feedback são exibidas para o usuário através de um componente de diálogo.

## Estrutura do Projeto

### Módulos

- **HomeModule**: Módulo `Home` que inicializa a aplicação.

  - **HomeComponent**: Componente `Home` responsável por inicializar a aplicação.

- **TasksModule**: Módulo `Tasks` que gerencia as funcionalidades de gerenciamento de tarefas.

  - **TaskListComponent**: Componente para listar as tarefas.
  - **AddTaskComponent**: Componente para adicionar novas tarefas.
  - **EditTaskComponent**: Componente para editar tarefas existentes.
  - **TaskItemComponent**: Componente para representar um item de tarefa na lista.
  - **TaskDialogComponent**: Componente de diálogo para confirmação de exclusão de tarefas.

- **NavBarModule**: Módulo que contém o componente de barra de navegação.

  - **NavBarComponent**: Componente de barra de navegação.

- **MessageDialogModule**: Módulo que contém o componente de diálogo de mensagens.
  - **MessageDialogComponent**: Componente de diálogo para exibir mensagens de sucesso e erro.

## Gerenciamento de Estado com BehaviorSubject

O projeto utiliza o padrão BehaviorSubject para gerenciar o estado das tarefas. Isso permite uma comunicação eficiente entre os componentes da aplicação, garantindo que todas as partes interessadas sejam notificadas sobre as mudanças no estado das tarefas em tempo real.

### Implementação no `TaskService`

O serviço `TaskService` utiliza o BehaviorSubject `tasksSubject` para manter o estado atual das tarefas. Este BehaviorSubject é então exposto como um Observable através de `tasksState`, permitindo que os componentes se inscrevam para receber atualizações sempre que o estado das tarefas mudar.

## Uso de `<dialog>` com `ngSwitch`

O componente `TaskDialogComponent` utiliza o elemento `<dialog>` do HTML5 juntamente com a diretiva `ngSwitch` para alternar entre os modos de adição e edição de tarefas. Isso proporciona uma experiência interativa ao usuário, exibindo o formulário de edição ou adição conforme necessário.

## Outros Aspectos Destacados

- **Uso de Reactive Forms**: O projeto utiliza Angular Reactive Forms para gerenciar formulários, garantindo validação e controle de estado de forma eficiente.
- **Boas Práticas de Componentização**: Componentes estruturados e modularizados, seguindo boas práticas de desenvolvimento Angular. Cada componente tem uma responsabilidade clara e pode ser facilmente reutilizado.
- **Manuseio de Eventos e Emissão de Eventos**: Uso eficaz de EventEmitter para comunicar eventos de componentes filhos para componentes pais, melhorando a interatividade da aplicação.
- **Padrões de Design Responsivo**: Abordagem responsiva no design, para uma experiência do usuário consistente em diferentes dispositivos.
- **Utilização de Observables e Pipes**: Uso de Observables e pipes do RxJS, melhorando a eficiência da manipulação de dados assíncronos e a transformação de valores.

## Como Executar o Projeto

Para executar este projeto localmente, siga as etapas abaixo:

1. Clone este repositório para o seu ambiente local.
2. Instale as dependências necessárias utilizando o npm ou yarn.
3. Inicie o servidor de desenvolvimento local usando o comando `ng serve`.
4. Abra seu navegador e acesse `http://localhost:4200/` para visualizar a aplicação.
